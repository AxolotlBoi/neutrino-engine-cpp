#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
namespace NT
{
	class Transform
	{
	public:
		glm::vec3 pos;
		glm::mat4 rot;
		glm::vec3 scale = { 1.0f, 1.0f, 1.0f };

		glm::mat4 GetPositionMatrix();
		glm::mat4 GetScaleMatrix();

		glm::vec3 GetForward();
		glm::vec3 GetBackward();
		glm::vec3 GetRight();
		glm::vec3 GetLeft();
		glm::vec3 GetUp();
		glm::vec3 GetDown();

		glm::mat4 GetMatrix();
	private:
	};
}
