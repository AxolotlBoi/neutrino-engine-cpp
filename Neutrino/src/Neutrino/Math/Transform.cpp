#include "ntpch.h"
#include "Transform.h"

namespace NT
{
	glm::mat4 Transform::GetPositionMatrix()
	{
		glm::mat4 PositionMatrix = glm::translate(glm::mat4(1.0f), pos);
		return PositionMatrix;
	}

	glm::mat4 Transform::GetScaleMatrix()
	{
		glm::mat4 ScaleMatrix = glm::scale(glm::mat4(1.0f), scale);
		return ScaleMatrix;
	}

	glm::vec3 Transform::GetForward()
	{
		glm::vec3 dir = rot * glm::vec4(0, 0, -1, 0);
		return dir;
	}

	glm::vec3 Transform::GetBackward()
	{
		glm::vec3 dir = rot * glm::vec4(0, 0, 1, 0);
		return dir;
	}

	glm::vec3 Transform::GetRight()
	{
		glm::vec3 dir = rot * glm::vec4(1, 0, 0, 0);
		return dir;
	}

	glm::vec3 Transform::GetLeft()
	{
		glm::vec3 dir = rot * glm::vec4(-1, 0, 0, 0);
		return dir;
	}

	glm::vec3 Transform::GetUp()
	{
		glm::vec3 dir = rot * glm::vec4(0, 1, 0, 0);
		return dir;
	}

	glm::vec3 Transform::GetDown()
	{
		glm::vec3 dir = rot * glm::vec4(0, -1, 0, 0);
		return dir;
	}
	glm::mat4 Transform::GetMatrix()
	{
		return GetPositionMatrix() * (rot * GetScaleMatrix());
	}
}