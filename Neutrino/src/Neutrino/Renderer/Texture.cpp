#include "ntpch.h"
#include "Texture.h"

#include "Renderer.h"
#include "Platform/OpenGL/OpenGLTexture.h"

namespace NT
{
	Ref<Texture2D> Texture2D::Create(const std::string& path)
	{
		switch (Renderer::GetAPI())
		{
			case RendererAPI::API::None: NT_CORE_ASSERT(false, "RendererAPI::None is currently unavailable!"); return nullptr;
			case RendererAPI::API::OpenGL: return std::make_shared<OpenGLTexture2D>(path);
		}

		NT_CORE_ASSERT(false, "Unknown RendererAPI");
		return nullptr;
	}
}