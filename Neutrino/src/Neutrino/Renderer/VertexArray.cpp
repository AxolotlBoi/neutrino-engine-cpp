#include "ntpch.h"
#include "VertexArray.h"
#include "Renderer.h"
#include "Platform/OpenGL/OpenGLVertexArray.h"

namespace NT
{
	VertexArray* VertexArray::Create()
	{
		switch (Renderer::GetAPI())
		{
			case RendererAPI::API::None: NT_CORE_ASSERT(false, "RendererAPI::None is currently unavailable!"); return nullptr;
			case RendererAPI::API::OpenGL: return new OpenGLVertexArray();
		}

		NT_CORE_ASSERT(false, "Unknown RendererAPI");
		return nullptr;
	}
}