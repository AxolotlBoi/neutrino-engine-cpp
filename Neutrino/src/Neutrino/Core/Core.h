#pragma once

#include <memory>

#ifdef NT_PLATFORM_WINDOWS

#if NT_DYNAMIC_LINK
	#ifdef NT_BUILD_DLL
		#define NEUTRINO_API _declspec(dllexport)
	#else
		#define NEUTRINO_API _declspec(dllimport)
	#endif
#else
	#define NEUTRINO_API
#endif
#else
	#error Neutrino Only Supports Windows!
#endif

#ifdef NT_DEBUG
	#define NT_ENABLE_ASSERTS
#endif

#ifdef NT_ENABLE_ASSERTS
	#define NT_ASSERT(x, ...) { if(!(x)) { NT_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
	#define NT_CORE_ASSERT(x, ...) { if(!(x)) { NT_CORE_ERROR("Assertion Failed: {0}", __VA_ARGS__); __debugbreak(); } }
#else
	#define NT_ASSERT(x, ...)
	#define NT_CORE_ASSERT(x, ...)
#endif

#define BIT(x) (1 << x)

#define NT_BIND_EVENT_FN(fn) std::bind(&fn, this, std::placeholders::_1)

namespace NT
{
	template<typename T>
	using Scope = std::unique_ptr<T>;

	template<typename T>
	using Ref = std::shared_ptr<T>;
}