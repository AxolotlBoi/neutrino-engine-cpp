#pragma once

#include "Core.h"
#include "Neutrino/Events/Event.h"
#include "Neutrino/Events/ApplicationEvent.h"
#include "Neutrino/Core/LayerStack.h"
#include "Window.h"
#include "Neutrino/Renderer/Shader.h"
#include "Neutrino/Renderer/Buffer.h"
#include "Neutrino/Renderer/VertexArray.h"

#include "Neutrino/Core/Timestep.h"

namespace NT
{
	class NEUTRINO_API Application
	{
	public:
			Application();
			virtual ~Application();

			void Run();

			void OnEvent(Event& e);

			void PushLayer(Layer* layer);
			void PushOverlay(Layer* layer);

			inline static Application& Get() { return *s_Instance; }
			inline Window& GetWindow() { return *m_Window; }
	private:
		bool OnWindowClose(WindowCloseEvent& e);
	private:
		std::unique_ptr<Window>  m_Window;
		bool m_Running = true;
		LayerStack m_LayerStack;
		float m_LastFrameRenderTime = 0.0f;
	private:
		static Application* s_Instance;
	};

	Application* CreateApplication();
}